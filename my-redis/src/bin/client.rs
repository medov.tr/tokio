use mini_redis::client;
use tokio::sync::mpsc;
use tokio::sync::oneshot;
use bytes::Bytes;

/// Multiple different commands are multiplexed over a single channel.
#[derive(Debug)]
enum Command {
    Get {
        key: String,
        resp: Responder<Option<Bytes>>,
    },
    Set {
        key: String,
        val: Bytes,
        resp: Responder<()>,
    },
}

type Responder<T> = oneshot::Sender<mini_redis::Result<T>>;

#[tokio::main]
async fn main() {
    // Create a new channel with a capacity of at most 32.
    let (tx, mut rx) = mpsc::channel(32);
    let tx2 = tx.clone();
    let tx3 = tx.clone();
    let tx4 = tx.clone();

    let manager = tokio::spawn(async move {
        // Establish a connection to the server
        let mut client = client::connect("127.0.0.1:6379").await.unwrap();
    
        // Start receiving messages
        while let Some(cmd) = rx.recv().await {
            use Command::*;
            match cmd {
                Get { key, resp } => {
                    let res = client.get(&key).await;
                    // Ignore errors
                    let _ = resp.send(res);
                }
                Set { key, val, resp } => {
                    let res = client.set(&key, val).await;
                    // Ignore errors
                    let _ = resp.send(res);
                }
            }
        }
    });

    let t1 = tokio::spawn(async move {
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = Command::Set {
            key: "hello".to_string(),
            val: "world".into(),
            resp: resp_tx,
        };
    
        // Send the SET request
        tx3.send(cmd).await.unwrap();
    
        // Await the response
        let res = resp_rx.await;
        println!("GOT = {:?}", res);
    });


    let t2 = tokio::spawn(async move {
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = Command::Get {
            key: "hello".to_string(),
            resp: resp_tx,
        };
    
        // Send the GET request
        tx.send(cmd).await.unwrap();
    
        // Await the response
        let res = resp_rx.await;
        println!("GOT = {:?}", res);
    });
    
    let t3 = tokio::spawn(async move {
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = Command::Set {
            key: "foo".to_string(),
            val: "bar".into(),
            resp: resp_tx,
        };
    
        // Send the SET request
        tx2.send(cmd).await.unwrap();
    
        // Await the response
        let res = resp_rx.await;
        println!("GOT = {:?}", res);
    });

    let t4 = tokio::spawn(async move {
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = Command::Get {
            key: "hi".to_string(),
            resp: resp_tx,
        };
    
        // Send the GET request
        tx4.send(cmd).await.unwrap();
    
        // Await the response
        let res = resp_rx.await;
        println!("GOT = {:?}", res);
    });

    //let (tx, rx) = oneshot::channel();
    t1.await.unwrap();
    t2.await.unwrap();
    t3.await.unwrap();
    t4.await.unwrap();
    manager.await.unwrap();
    
}