use tokio::fs::File;
use tokio::io::{self, AsyncReadExt, AsyncWriteExt};

#[tokio::main]
async fn main() {
    write_file().await.unwrap();
    read_file().await.unwrap();
    copy_file().await.unwrap();

}

async fn read_file() -> io::Result<()>{
    let mut f = File::open("foo.txt").await?;
    // let mut buffer = [0; 10];
    // // read up to 10 bytes
    // let n = f.read(&mut buffer[..]).await?;

    let mut buffer = Vec::new();
    // read the whole file
    f.read_to_end(&mut buffer).await?;
    println!("The bytes: {:?}", &buffer);
    Ok(())
}

async fn write_file() -> io::Result<()> {
    let mut file = File::create("foo.txt").await?;

    // Writes some prefix of the byte string, but not necessarily all of it.
    let n = file.write(b"hello").await?;
    //file.write_all(b"some bytes").await?;
    println!("Wrote the first {} bytes of 'some bytes'.", n);
    Ok(())
}

async fn copy_file() -> io::Result<()> {
    use tokio::io;
    let mut reader: &[u8] = b"hello";
    let mut file = File::create("foo_copy.txt").await?;
    io::copy(&mut reader, &mut file).await?;
    Ok(())
}
